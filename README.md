# 3D Basic test

basic 3d calculation for testing

### dependencies

> sfml in C:/3rdParty/

> add dll to build/debug directory

### compile with cmake

```cmake
mkdir build 
cd build

cmake -G "Visual Studio 16 2019" ..

cmake --build . --config Debug

Debug/3dTest.exe

```



#include "Floor.h"

Floor::Floor() {}
Floor::~Floor() {}

void Floor::selected(bool v, Vec4 hitPoint) {

  if ( ! StageInfo::get()->sheepSelected) {
    return;
  }

  Vec4 sp = StageInfo::get()->sheepPos;
  Vec4 dir = hitPoint - sp;
  dir.y = 0.f;
  Vec4 dirNormal = Utils::get()->getNormalize(dir);
  Vec4 origDir = { 0.f, 0.f, 1.f, 1.f }; 
  float dot = Utils::get()->getDotProduct(dirNormal, origDir);

  StageInfo::get()->aY = acos(dot) * (dir.x < 0.0f ? 1.0f : -1.0f);
}




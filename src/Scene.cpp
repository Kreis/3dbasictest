#include "Scene.h"

Scene::Scene() {}
Scene::~Scene() {}

void Scene::init(Canvas* canvas) {

  sheep.mesh.loadFromFile("data/sheepColored.obj", 2);
  sheep.position.z = 6.0;
  StageInfo::get()->sheepPos = sheep.position;

  floor.mesh.loadFromFile("data/floor.obj", 2);
  floor.position.z = 15;

  canvas->entities.push_back(&floor);
  canvas->entities.push_back(&sheep);  
}






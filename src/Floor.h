#ifndef _FLOOR_H_
#define _FLOOR_H_

#include <functional>
#include <math.h>

#include "Entity.h"
#include "StageInfo.h"

class Floor : public Entity {
public:
  Floor();
  ~Floor();

  void selected(bool v, Vec4 hitPoint) override;
};


#endif

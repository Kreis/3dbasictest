#ifndef _CANVAS_H_
#define _CANVAS_H_

#include <SFML/Graphics.hpp>

#include <iostream>
#include <math.h>
#include <vector>
#include <queue>
#include "StageInfo.h"
#include "Entity.h"
#include "Utils.h"


class Canvas {
public:
  Canvas();
  ~Canvas();

  void init(int32_t width, int32_t height);
  void update(float deltatime);
  void draw(sf::RenderWindow& rwindow);

  bool clipTriangle(
    const Vec4& P, const Vec4& N, std::queue<Triangle>& Q);

  void funToScreen(float x, float y, float& ox, float &oy);

  Vec2i screenSize; 

  std::vector<sf::Vertex> vertex;

  Vec4 cameraPos;
  Vec4 cameraDir;

  float screenAspect;

  float zNear = 0.1f;
  float zFar = 1000.0;
  float theta = 90.0 / 180.0 * 3.141592;

  bool pressLeft;
  bool pressRight;
  bool pressUp;
  bool pressDown;
  bool pressU;
  bool pressI;
  bool click;
  float mouseX;
  float mouseY;

  Vec4 cameraAng;

  bool cameraLeftPress;
  bool cameraRightPress;
  bool cameraUpPress;
  bool cameraDownPress;
  bool cameraForwardPress;
  bool cameraBackwardPress;
  bool cameraRotateLeftPress;
  bool cameraRotateRightPress;
  float speedCamera;

  Vec4 lightDirection;

  std::vector<Entity*> entities;

  bool hitSomething;
};
#endif

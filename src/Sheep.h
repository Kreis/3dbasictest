#ifndef _SHEEP_H_
#define _SHEEP_H_

#include "Entity.h"
#include "StageInfo.h"

class Sheep : public Entity {
public:
  Sheep();
  ~Sheep();

  void selected(bool v, Vec4 hitPoint) override;

};


#endif

#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "Utils.h"

class Entity {
public:
  virtual void selected(bool v, Vec4 hitPoint) = 0;

  Vec4 position;
  Vec4 angRot;
  Mesh mesh;
  float speed = 0.0f;
  float extraLight = 0.0f;
};

#endif

#ifndef _STAGEINFO_H_
#define _STAGEINFO_H_

#include "Utils.h"

class StageInfo {
public:
  ~StageInfo();
  
  bool sheepSelected = false;
  Vec4 sheepPos;
  float aY = 0.f;

  static StageInfo* get();

private:
  StageInfo();
  static StageInfo* instancePtr;
};

#endif

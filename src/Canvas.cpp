#include "Canvas.h"

Canvas::Canvas() {}
Canvas::~Canvas() {}

void Canvas::init(int32_t width, int32_t height) {

  std::cout << "init canvas" << std::endl;

  screenSize.x = width;
  screenSize.y = height;
  screenAspect = (float)screenSize.y / (float)screenSize.x;

  // mouse
  mouseX = 0.0;
  mouseY = 0.0;

  // camera
  // hardcoded isometric
  cameraPos = { 0.95011f, 3.11245f, 1.19082f, 1.0f };
  cameraAng = { -0.6664f, 0.1666f, 0.0f, 1.0f };
  cameraDir = { 0.0f, 0.0f, 1.0f, 0.0f };
  speedCamera = 0.0f;

  lightDirection = { 0.0f, 0.f, -1.f, 1.f };

  hitSomething = false;
}

void Canvas::update(float deltatime) {

  if (pressLeft) {
    StageInfo::get()->aY += 2.0f * deltatime;
  }
  if (pressRight) {
    StageInfo::get()->aY -= 2.0f * deltatime;
  }

  entities[ 1 ]->angRot.y = StageInfo::get()->aY;

  entities[ 1 ]->speed = 0.0f;
  if (pressUp) {
    entities[ 1 ]->speed = 2.0f * deltatime;
    entities[ 1 ]->selected(false, {0,0,0,0});
  }
  if (pressDown) {
    entities[ 1 ]->speed = -2.0f * deltatime;
    entities[ 1 ]->selected(false, {0,0,0,0});
  }
  if (pressU) {
    cameraAng.y -= deltatime;
  }
  if (pressI) {
    cameraAng.y += deltatime;
  }

  //camera
  if (cameraLeftPress) {
    cameraPos.x -= deltatime;
  }
  if (cameraRightPress) {
    cameraPos.x += deltatime;
  }
  if (cameraUpPress) {
    cameraPos.y += deltatime;
  }
  if (cameraDownPress) {
    cameraPos.y -= deltatime;
  }
  if (cameraForwardPress) {
    cameraPos.z += deltatime;
  }
  if (cameraBackwardPress) {
    cameraPos.z -= deltatime;
  }
  if (cameraRotateLeftPress) {
    cameraAng.x -= deltatime;
  }
  if (cameraRotateRightPress) {
    cameraAng.x += deltatime;
  }
  Mat4 cameraMatRotX = Utils::get()->getMatRotationX(cameraAng.x);
  Mat4 cameraMatRotY = Utils::get()->getMatRotationY(cameraAng.y);
  Mat4 cameraMatRotZ = Utils::get()->getMatRotationZ(cameraAng.z);
  Mat4 cameraMatRot = Utils::get()->multiplyMxM(cameraMatRotX, cameraMatRotY);
  cameraMatRot = Utils::get()->multiplyMxM(cameraMatRot, cameraMatRotZ);

  Vec4 modifCameraDir = Utils::get()->multiplyVxM(cameraDir, cameraMatRot);
  Vec4 vecSpeed = modifCameraDir * speedCamera;
  cameraPos = cameraPos + vecSpeed;

  Vec4 cameraTarget = cameraPos + modifCameraDir;
  Vec4 cameraUp = { 0.0, 1.0, 0.0, 0.0 };
  Mat4 lookAtMat = Utils::get()->getMatLookAt(
    cameraPos, cameraTarget, cameraUp); 
  Mat4 pointAtMat = Utils::get()->getMatPointAt(
    cameraPos, cameraTarget, cameraUp);

  if (click) {
    hitSomething = false;
  }
  vertex.clear();
  // transform each entity
  for (Entity* entity : entities ) {
    
    Mat4 rotateMatX = Utils::get()->getMatRotationX(entity->angRot.x);
    Mat4 rotateMatY = Utils::get()->getMatRotationY(entity->angRot.y);
    Mat4 rotateMatZ = Utils::get()->getMatRotationZ(entity->angRot.z);

    Mat4 rotateMat = Utils::get()->multiplyMxM(rotateMatX, rotateMatY);
    rotateMat = Utils::get()->multiplyMxM(rotateMat, rotateMatZ);

    Vec4 entityForward = { 0.0f, 0.0f, 1.0f, 1.0f };
    entityForward = Utils::get()->multiplyVxM(entityForward, rotateMat);
    entityForward = Utils::get()->getNormalize(entityForward);
    entityForward = entityForward * entity->speed;

    entity->position = entity->position + entityForward;

    std::vector<Triangle> modifMesh;
    for (int32_t i = 0; i < entity->mesh.data.size(); i++) {
      // rotation
      Triangle tri = entity->mesh.data[ i ];
      tri.p[ 0 ] = Utils::get()->multiplyVxM(tri.p[ 0 ], rotateMat);
      tri.p[ 1 ] = Utils::get()->multiplyVxM(tri.p[ 1 ], rotateMat);
      tri.p[ 2 ] = Utils::get()->multiplyVxM(tri.p[ 2 ], rotateMat);

      // position in space
      tri.p[ 0 ] = tri.p[ 0 ] + entity->position;
      tri.p[ 1 ] = tri.p[ 1 ] + entity->position;
      tri.p[ 2 ] = tri.p[ 2 ] + entity->position;
      
      // locate relative to camera
      tri.p[ 0 ] = Utils::get()->multiplyVxM(tri.p[ 0 ], lookAtMat);
      tri.p[ 1 ] = Utils::get()->multiplyVxM(tri.p[ 1 ], lookAtMat);
      tri.p[ 2 ] = Utils::get()->multiplyVxM(tri.p[ 2 ], lookAtMat);

      // clip when is too close to camera
      std::queue<Triangle> Q;
      Q.push(tri);
      clipTriangle(
        { 0.0, 0.0, 1.0, 1.0 },
        { 0.0, 0.0, 1.0, 1.0 },
        Q);
      while ( ! Q.empty()) {
        modifMesh.push_back(Q.front());
        Q.pop();
      }
    }

    std::vector<Triangle> modifHitBox;
    for (int32_t i = 0; i < entity->mesh.hitBox.size(); i++) {
      // rotation
      Triangle tri = entity->mesh.hitBox[ i ];
      tri.p[ 0 ] = Utils::get()->multiplyVxM(tri.p[ 0 ], rotateMat);
      tri.p[ 1 ] = Utils::get()->multiplyVxM(tri.p[ 1 ], rotateMat);
      tri.p[ 2 ] = Utils::get()->multiplyVxM(tri.p[ 2 ], rotateMat);

      // position in space
      tri.p[ 0 ] = tri.p[ 0 ] + entity->position;
      tri.p[ 1 ] = tri.p[ 1 ] + entity->position;
      tri.p[ 2 ] = tri.p[ 2 ] + entity->position;
      
      // locate relative to camera
      tri.p[ 0 ] = Utils::get()->multiplyVxM(tri.p[ 0 ], lookAtMat);
      tri.p[ 1 ] = Utils::get()->multiplyVxM(tri.p[ 1 ], lookAtMat);
      tri.p[ 2 ] = Utils::get()->multiplyVxM(tri.p[ 2 ], lookAtMat);

      modifHitBox.push_back(tri);
    }

    // Calculate here the raycast from mouse to cube hitbox
    if (click) {
      std::cout << "click" << std::endl;
      std::cout << mouseX << " " << mouseY << std::endl;
  
      std::cout << "camera pos: " <<
        cameraPos.x << " " << 
        cameraPos.y << " " << 
        cameraPos.z << " " << std::endl;

      Vec4 rayDirection;
      rayDirection.x = (mouseX / screenSize.x) * 2.0f - 1.0f;
      rayDirection.x /= screenAspect;
      rayDirection.y = (mouseY / screenSize.y) * 2.0f - 1.0f;
      rayDirection.y *= -1;
      rayDirection.z = 1.f;
      rayDirection = Utils::get()->getNormalize(rayDirection);
      float t;
      for (const Triangle& tri : modifHitBox) {
        bool collide = Utils::get()->rayTriangleIntersect(
          { 0, 0, 0, 0 }, rayDirection,
          tri.p[ 0 ],
          tri.p[ 1 ],
          tri.p[ 2 ], t);
        if (collide) {
          std::cout << "collide" << std::endl;
          float distance = sqrt(
            rayDirection.x*rayDirection.x +
            rayDirection.y*rayDirection.y +
            rayDirection.z*rayDirection.z) * t;
          Vec4 hitPoint = {
            rayDirection.x * t,
            rayDirection.y * t,
            rayDirection.z * t };
          hitPoint = Utils::get()->multiplyVxM(hitPoint, pointAtMat);
          std::cout << "hit points moved: " << std::endl;
          std::cout << "x: " << hitPoint.x << std::endl;
          std::cout << "z: " << hitPoint.z << std::endl;

          entity->selected(true, hitPoint);
          hitSomething = true;
        }
      }
    }


    // sort triangles by Z axis (average)
    std::sort(modifMesh.begin(), modifMesh.end(),
    [](const Triangle& A, const Triangle& B) {
      return (A.p[ 0 ].z + A.p[ 1 ].z + A.p[ 2 ].z) > 
        (B.p[ 0 ].z + B.p[ 1 ].z + B.p[ 2 ].z);
    });

    // project 3D -> 2D
    Mat4 matProj = Utils::get()->getMatProjection(
      screenAspect, theta, zNear, zFar);

    for (int32_t i = 0; i < modifMesh.size(); i++) {
      Triangle& tri = modifMesh[ i ];

      // verify if visible from 0,0,0
      Vec4 line1 = tri.p[ 1 ] - tri.p[ 0 ];
      Vec4 line2 = tri.p[ 2 ] - tri.p[ 0 ];

      Vec4 normalV = Utils::get()->getNormalVector(line1, line2);
      normalV = Utils::get()->getNormalize(normalV);

      Vec4 dirPoint0 = Utils::get()->getNormalize(tri.p[ 0 ]);
      bool visible = Utils::get()->getDotProduct(normalV, dirPoint0) < 0.0;
      if ( ! visible) {
        continue;
      }

      // calculate light as light direction
      lightDirection = Utils::get()->getNormalize(lightDirection);
      float lighty = Utils::get()->getDotProduct(normalV, lightDirection);

      // point to projection in screen
      tri.p[ 0 ] = Utils::get()->multiplyVxM(tri.p[ 0 ], matProj);
      tri.p[ 1 ] = Utils::get()->multiplyVxM(tri.p[ 1 ], matProj);
      tri.p[ 2 ] = Utils::get()->multiplyVxM(tri.p[ 2 ], matProj);

      // clipping outside of screen
      std::queue<Triangle> Q;
      Q.push(tri);
      clipTriangle(
        { -1.0f, 0.0f, 0.0f, 1.0f },
        { 1.0f, 0.0f, 0.0f, 1.0f }, Q) ? 1 : 0;
      clipTriangle(
        { 1.0f, 0.0f, 0.0f, 1.0f },
        { -1.0f, 0.0f, 0.0f, 1.0f }, Q) ? 1 : 0;
      clipTriangle(
        { 0.0f, -1.0f, 0.0f, 1.0f },
        { 0.0f, 1.0f, 0.0f, 1.0f }, Q) ? 1 : 0;
      clipTriangle(
        { 0.0f, 1.0f, 0.0f, 1.0f },
        { 0.0f, -1.0f, 0.0f, 1.0f }, Q) ? 1 : 0;

      // Add vertex from triangles
      while ( ! Q.empty()) {

        Triangle& viewTri = Q.front();

        float fx, fy;
        for (int32_t i = 0; i < 3; i++) {
          funToScreen(viewTri.p[ i ].x, viewTri.p[ i ].y, fx, fy);
          sf::Vertex v;
          v.position.x = fx;
          v.position.y = fy;
          lighty = lighty > 0.05f ? lighty : 0.05f;
          lighty += entity->extraLight;
          if (lighty > 1.0f) {
            lighty = 1.0f;
          }
          v.color = sf::Color(
            lighty * viewTri.color.x * 255.f,
            lighty * viewTri.color.y * 255.f,
            lighty * viewTri.color.z * 255.f,
            255);
          vertex.push_back(v);
        }

        Q.pop();
      }
    }

  } // end for each entity
  if ( ! hitSomething) {
    for (Entity* entity : entities) {
      entity->selected(false, { 0.f, 0.f, 0.f, 0.f });
    }
  }

}

void Canvas::draw(sf::RenderWindow& rwindow) {
  if (vertex.size() > 0) {
    rwindow.draw(&vertex[ 0 ], vertex.size(), sf::Triangles);
  }
}

void Canvas::funToScreen(float x, float y, float& ox, float &oy) {
  ox = (x + 1.0) * ((float)screenSize.x * 0.5);
  oy = (-y + 1.0) * ((float)screenSize.y * 0.5);
}

bool Canvas::clipTriangle(
  const Vec4& P, const Vec4& N, std::queue<Triangle>& Q) {

  bool doClip = false;
  int32_t Qnum = Q.size();
  while (Qnum-->0) {
    Triangle tri = Q.front();
    Q.pop();

    float d1 = Utils::get()->getDotProduct(tri.p[ 0 ] - P, N);
    float d2 = Utils::get()->getDotProduct(tri.p[ 1 ] - P, N);
    float d3 = Utils::get()->getDotProduct(tri.p[ 2 ] - P, N);

    if (d1 >= 0.0 && d2 >= 0.0 && d3 >= 0.0) {
        // whole triangle inside
        Q.push(tri);
        continue;
    }

    if (d1 < 0.0 && d2 < 0.0 && d3 < 0.0) {
        // whole triangle outside
        continue;
    }

    doClip = true;

    int32_t insidePoints = 0;
    insidePoints += (d1 >= 0.0) ? 1 : 0;
    insidePoints += (d2 >= 0.0) ? 1 : 0;
    insidePoints += (d3 >= 0.0) ? 1 : 0;

    if (insidePoints == 1) {

      Triangle newTri;
      newTri.color = tri.color;
      int8_t newI = 0;
      if (d1 >= 0.0) {
        newTri.p[ newI ] = tri.p[ 0 ];
        newI++;
      }

      if (d1 * d2 < 0.0) {
        newTri.p[ newI ] =
          tri.p[ 0 ] + (tri.p[ 1 ] - tri.p[ 0 ]) *
          (std::abs(d1) / (std::abs(d1) + std::abs(d2)));
        newI++;
      }

      if (d2 >= 0.0) {
        newTri.p[ newI ] = tri.p[ 1 ];
        newI++;
      }

      if (newI == 3) {
        Q.push(newTri);
        continue;
      }
  
      if (d2 * d3 < 0.0) {
        newTri.p[ newI ] =
          tri.p[ 1 ] + (tri.p[ 2 ] - tri.p[ 1 ]) *
          (std::abs(d2) / (std::abs(d2) + std::abs(d3)));
        newI++;
      }

      if (newI == 3) {
        Q.push(newTri);
        continue;
      }

      if (d3 >= 0.0) {
        newTri.p[ newI ] = tri.p[ 2 ];
        newI++;
      }

      if (newI == 3) {
        Q.push(newTri);
        continue;
      }

      if (d3 * d1 < 0.0) {
        newTri.p[ newI ] =
          tri.p[ 2 ] + (tri.p[ 0 ] - tri.p[ 2 ]) *
          (std::abs(d3) / (std::abs(d3) + std::abs(d1)));
        newI++;
      }

      if (newI == 3) {
        Q.push(newTri);
        continue;
      }

      // run until here shouldn't be possible
      std::cout << "ERROR, clipping is not working" << std::endl;
    }

    if (insidePoints == 2) {

      Triangle newTris[ 2 ];
      newTris[ 0 ].color = tri.color;
      newTris[ 1 ].color = tri.color;
      int8_t trisI = 0;
      int8_t newI = 0;
      if (d1 >= 0.0) {
        newTris[ trisI ].p[ newI ] = tri.p[ 0 ];
        newI++;
      }

      if (d1 * d2 < 0.0) {
        newTris[ trisI ].p[ newI ] =
          tri.p[ 0 ] + (tri.p[ 1 ] - tri.p[ 0 ]) *
          (std::abs(d1) / (std::abs(d1) + std::abs(d2)));
        newI++;
      }

      if (d2 >= 0.0) {
        newTris[ trisI ].p[ newI ] = tri.p[ 1 ];
        newI++;
      }

      if (newI == 3) {
        Q.push(newTris[ 0 ]);
        newTris[ 1 ].p[ 0 ] = newTris[ 0 ].p[ 2 ];
        trisI = 1;
      }
  
      if (d2 * d3 < 0.0) {
        Vec4 d2d3 = tri.p[ 1 ] + (tri.p[ 2 ] - tri.p[ 1 ]) *
          (std::abs(d2) / (std::abs(d2) + std::abs(d3)));

        if (trisI == 1) {
          newTris[ 1 ].p[ 1 ] = d2d3;
          newTris[ 1 ].p[ 2 ] = newTris[ 0 ].p[ 0 ];
          Q.push(newTris[ 1 ]);
          continue;
        }

        newTris[ 0 ].p[ newI ] = d2d3;
        newI++;
      }

      if (newI == 3) {
        Q.push(newTris[ 0 ]);
        newTris[ 1 ].p[ 0 ] = newTris[ 0 ].p[ 2 ];
        trisI = 1;
      }

      if (d3 >= 0.0) {
        if (trisI == 1) {
          newTris[ 1 ].p[ 1 ] = tri.p[ 2 ];
          newTris[ 1 ].p[ 2 ] = newTris[ 0 ].p[ 0 ];
          Q.push(newTris[ 1 ]);
          continue;
        }

        newTris[ 0 ].p[ newI ] = tri.p[ 2 ];
        newI++;
      }

      if (newI == 3) {
        Q.push(newTris[ 0 ]);
        newTris[ 1 ].p[ 0 ] = newTris[ 0 ].p[ 2 ];
        trisI = 1;
      }

      if (d3 * d1 < 0.0) {
        Vec4 d3d1 = tri.p[ 2 ] + (tri.p[ 0 ] - tri.p[ 2 ]) *
          (std::abs(d3) / (std::abs(d3) + std::abs(d1)));

        if (trisI == 1) {
          newTris[ 1 ].p[ 1 ] = d3d1;
          newTris[ 1 ].p[ 2 ] = newTris[ 0 ].p[ 0 ];
          Q.push(newTris[ 1 ]);
          continue;
        }
      }

      // run until here shoulnd't be possible
      std::cout << "ERROR2 clipping is not working" << std::endl;
    }
  }
  
  return doClip;
}





















#ifndef _SCENE_H_
#define _SCENE_H_

#include <iostream>

#include "Canvas.h"
#include "Sheep.h"
#include "StageInfo.h"
#include "Floor.h"

class Scene {
public:
  Scene();
  ~Scene();

  void init(Canvas* canvas);

  Sheep sheep;
  Floor floor;
};


#endif

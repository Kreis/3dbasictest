#include "StageInfo.h"

StageInfo::StageInfo() {}
StageInfo::~StageInfo() {
  if (instancePtr != nullptr) {
    delete instancePtr;
  }
}

StageInfo* StageInfo::instancePtr = nullptr;
StageInfo* StageInfo::get() {
  if (instancePtr == nullptr) {
    instancePtr = new StageInfo();
  }
  return instancePtr;
}

#include <iostream>
#include <SFML/Graphics.hpp>

#include "Canvas.h"
#include "Scene.h"

int main(int arg, char** argv) {

  int32_t width = 1200;
  int32_t height = 800;
  sf::RenderWindow rwindow(sf::VideoMode(width, height), "3d Test");
  rwindow.setVerticalSyncEnabled(true);

  sf::Event ev;

  Canvas canvas;
  canvas.init(width, height);

  Scene scene;
  scene.init(&canvas);

  while (rwindow.isOpen()) {

    canvas.pressLeft = false;
    canvas.pressRight = false;
    canvas.pressUp = false;
    canvas.pressDown = false;
    canvas.pressU = false;
    canvas.pressI = false;
    canvas.click = false;
    canvas.mouseX = 0.0;
    canvas.mouseY = 0.0;
    canvas.cameraLeftPress = false;
    canvas.cameraRightPress = false;
    canvas.cameraUpPress = false;
    canvas.cameraDownPress = false;
    canvas.cameraForwardPress = false;
    canvas.cameraBackwardPress = false;
    canvas.cameraRotateLeftPress = false;
    canvas.cameraRotateRightPress = false;


    while (rwindow.pollEvent(ev)) {
      switch (ev.type) {
      case sf::Event::Closed:
        rwindow.close();
        break;

      case sf::Event::KeyPressed:
        if (ev.key.code == sf::Keyboard::Key::W) {
          rwindow.close();
        }
        canvas.pressLeft = ev.key.code == sf::Keyboard::Key::Left;
        canvas.pressRight = ev.key.code == sf::Keyboard::Key::Right;
        canvas.pressUp = ev.key.code == sf::Keyboard::Key::Up;
        canvas.pressDown = ev.key.code == sf::Keyboard::Key::Down;
        canvas.pressU = ev.key.code == sf::Keyboard::Key::U;
        canvas.pressI = ev.key.code == sf::Keyboard::Key::I;
        canvas.cameraLeftPress = ev.key.code == sf::Keyboard::Key::Z;
        canvas.cameraRightPress = ev.key.code == sf::Keyboard::Key::C;
        canvas.cameraUpPress = ev.key.code == sf::Keyboard::Key::S;
        canvas.cameraDownPress = ev.key.code == sf::Keyboard::Key::X;
        canvas.cameraForwardPress = ev.key.code == sf::Keyboard::Key::G;
        canvas.cameraBackwardPress = ev.key.code == sf::Keyboard::Key::H;
        canvas.cameraRotateLeftPress = ev.key.code == sf::Keyboard::Key::T;
        canvas.cameraRotateRightPress = ev.key.code == sf::Keyboard::Key::Y;

        break;

      case sf::Event::MouseButtonPressed:
        canvas.click = ev.mouseButton.button == sf::Mouse::Button::Left;
        if (canvas.click) {
          sf::Vector2i mpos = sf::Mouse::getPosition(rwindow);
          canvas.mouseX = (float)mpos.x;
          canvas.mouseY = (float)mpos.y;
        }
      break;
      }

    }

    canvas.update(0.1666);

    rwindow.clear(sf::Color(120, 120, 120, 255));
    canvas.draw(rwindow);
    rwindow.display();

  }


  return 0;
}



#ifndef _UTILS_H_
#define _UTILS_H_

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <math.h>

struct Mat4 {
  float m[4][4];
};

struct Vec2i {
  int32_t x;
  int32_t y;
};

struct Vec4 {
  float x = 0.0;
  float y = 0.0;
  float z = 0.0;
  float w = 1.0;

  Vec4 operator-(const Vec4& other) const {
    return { x - other.x, y - other.y, z - other.z, w };
  }

  Vec4 operator*(const float& v) const {
    return {
      x * v, y * v, z * v, w
    };
  }
  
  Vec4 operator+(const Vec4& other) const {
    return {
      x + other.x, y + other.y, z + other.z, w
    };
  }
};

struct Triangle {
  Vec4 p[3];
  Vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
};

struct Mesh {
  std::vector<Triangle> data;
  std::vector<Triangle> hitBox;
  void loadFromFile(std::string path, int32_t version);

  void getOnlyVertex(std::string data, int& v);
};

class Utils {
public:
  ~Utils();

  static Utils* get();
  
  Mat4 multiplyMxM(const Mat4& A, const Mat4& B);
  Vec4 multiplyVxM(const Vec4& A, const Mat4& B);
  float getDotProduct(const Vec4& A, const Vec4& B);
  Vec4 getNormalVector(const Vec4& A, const Vec4& B);
  Vec4 getNormalize(const Vec4& A);
  bool rayTriangleIntersect(const Vec4& rayOrigin,
    const Vec4& rayDirection, const Vec4& v0,
    const Vec4& v1, const Vec4& v2, float& t);
  Mat4 Utils::getMatProjectionInverse(
    float aspect,
    float viewAngle,
    float zNear,
    float zFar);

  Mat4 getMatRotationX(float theta);
  Mat4 getMatRotationY(float theta);
  Mat4 getMatRotationZ(float theta);
  Mat4 getMatProjection(
    float aspect,
    float viewAngle,
    float zNear,
    float zFar);
  Mat4 getMatPointAt(
    const Vec4& pos, const Vec4& target, const Vec4& up);
  Mat4 getMatLookAt(
    const Vec4& pos, const Vec4& target, const Vec4& up);

private:
  Utils();
  static Utils* instancePtr;
};

#endif

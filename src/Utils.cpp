#include "Utils.h"

Utils* Utils::instancePtr = nullptr;

Utils::Utils() {}
Utils::~Utils() {
  if (instancePtr != nullptr)
    delete instancePtr;
}

Utils* Utils::get() {
  if (instancePtr == nullptr) {
    instancePtr = new Utils();
  }

  return instancePtr;
}

Mat4 Utils::multiplyMxM(const Mat4& A, const Mat4& B) {

  Mat4 C;

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      C.m[i][j] = 0;
    }
  }

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      for (int k = 0; k < 4; k++) {
        C.m[i][j] += A.m[i][k] * B.m[k][j];
      }
    }
  }

  return C;
}

Vec4 Utils::multiplyVxM(const Vec4& A, const Mat4& B) {
  Vec4 C;

  C.x = A.x * B.m[0][0] + A.y * B.m[1][0] + A.z * B.m[2][0] + A.w * B.m[3][0];
  C.y = A.x * B.m[0][1] + A.y * B.m[1][1] + A.z * B.m[2][1] + A.w * B.m[3][1];
  C.z = A.x * B.m[0][2] + A.y * B.m[1][2] + A.z * B.m[2][2] + A.w * B.m[3][2];
  C.w = A.x * B.m[0][3] + A.y * B.m[1][3] + A.z * B.m[2][3] + A.w * B.m[3][3];

  if (C.w != 0.0) {
    C.x /= C.w;
    C.y /= C.w;
    C.z /= C.w;
  }

  return C;
}

float Utils::getDotProduct(const Vec4& A, const Vec4& B) {
  float dotProduct = 0.0f;
  dotProduct += A.x * B.x;
  dotProduct += A.y * B.y;
  dotProduct += A.z * B.z;
  // dotProduct += A.w * B.w;

  return dotProduct;
}

Vec4 Utils::getNormalVector(const Vec4& A, const Vec4& B) {
  Vec4 normal;
  normal.x = (A.y * B.z) - (A.z * B.y);
  normal.y = (A.z * B.x) - (A.x * B.z);
  normal.z = (A.x * B.y) - (A.y * B.x);
  normal.w = 0.0f; // not used

  return normal;
}

Vec4 Utils::getNormalize(const Vec4& A) {
  Vec4 normalized;
  float magnitude = sqrt(A.x * A.x + A.y * A.y + A.z * A.z);
  normalized.x = A.x / magnitude;
  normalized.y = A.y / magnitude;
  normalized.z = A.z / magnitude;
  normalized.w = A.w;

  return normalized;
}

Mat4 Utils::getMatRotationX(float theta) {
  return {{
    {1, 0, 0, 0},
    {0, cos(theta), -sin(theta), 0},
    {0, sin(theta), cos(theta), 0},
    {0, 0, 0, 1}
  }};
}

Mat4 Utils::getMatRotationY(float theta) {
  return {{
    {cos(theta), 0, sin(theta), 0},
    {0, 1, 0, 0},
    {-sin(theta), 0, cos(theta), 0},
    {0, 0, 0, 1}
  }};
}

Mat4 Utils::getMatRotationZ(float theta) {
  return {{
    {cos(theta), -sin(theta), 0, 0},
    {sin(theta), cos(theta), 0, 0},
    {0, 0, 1, 0},
    {0, 0, 0, 1}
  }};
}

Mat4 Utils::getMatProjection(
  float aspect,
  float viewAngle,
  float zNear,
  float zFar) {

  float f = 1.0 / tan(viewAngle / 2.0);
  float q = zFar / (zFar - zNear);
  return {{
    { aspect * f,  0.0,   0.0,       0.0 },
    { 0.0,         f,     0.0,       0.0 },
    { 0.0,         0.0,   q,         1.0 },
    { 0.0,         0.0,   -zNear * q, 0.0 }
  }};
}

Mat4 Utils::getMatPointAt(
  const Vec4& pos, const Vec4& target, const Vec4& up) {

  Vec4 newForward = target - pos;
  newForward = getNormalize(newForward);

  Vec4 newUp = newForward * getDotProduct(up, newForward);
  newUp = up - newUp;
  newUp = getNormalize(newUp);

  Vec4 newRight = getNormalVector(up, newForward);
  newRight = getNormalize(newRight);

  Vec4 T = pos;
  
  Vec4 A = newRight;
  Vec4 B = newUp;
  Vec4 C = newForward;

  return {{
    { A.x, A.y, A.z, 0.0f },
    { B.x, B.y, B.z, 0.0f },
    { C.x, C.y, C.z, 0.0f },
    { T.x, T.y, T.z, 1.0f }
  }};
};

Mat4 Utils::getMatLookAt(
  const Vec4& pos, const Vec4& target, const Vec4& up) {
  
  Vec4 newForward = target - pos;
  newForward = getNormalize(newForward);

  Vec4 newUp = newForward * getDotProduct(up, newForward);
  newUp = up - newUp;
  newUp = getNormalize(newUp);

  Vec4 newRight = getNormalVector(up, newForward);
  newRight = getNormalize(newRight);

  Vec4 T = pos;
  
  Vec4 A = newRight;
  Vec4 B = newUp;
  Vec4 C = newForward;

  float TA = getDotProduct(T, A);
  float TB = getDotProduct(T, B);
  float TC = getDotProduct(T, C);
  return {{
    { A.x, B.x, C.x, 0.0f },
    { A.y, B.y, C.y, 0.0f },
    { A.z, B.z, C.z, 0.0f },
    { -TA, -TB, -TC, 1.0f }
  }};
};

// TODO not used
Mat4 Utils::getMatProjectionInverse(
  float aspect,
  float viewAngle,
  float zNear,
  float zFar) {
  
  float f = 1.0f / tan(viewAngle / 2.0);
  float q = zFar / (zFar - zNear);
  float det = aspect * f * q;
  
  return {{
    { 1.0f / (aspect * f), 0.0, 0.0, 0.0 },
    { 0.0, 1.0f / f, 0.0, 0.0 },
    { 0.0, 0.0, 0.0, 1.0f / zNear },
    { 0.0, 0.0, q / det, -zNear / det }
  }};
}

bool Utils::rayTriangleIntersect(
  const Vec4& rayOrigin, const Vec4& rayDirection,
  const Vec4& v0, const Vec4& v1, const Vec4& v2, float& t) {

  const float EPSILON = 0.000001;

  Vec4 edge1 = v1 - v0;
  Vec4 edge2 = v2 - v0;

  Vec4 pvec = getNormalVector(rayDirection, edge2);
  float det = getDotProduct(edge1, pvec);

  if (det > -EPSILON && det < EPSILON)
      return false;

  float inv_det = 1 / det;

  Vec4 tvec = rayOrigin - v0;
  float u = getDotProduct(tvec, pvec) * inv_det;

  if (u < 0 || u > 1)
      return false;

  Vec4 qvec = getNormalVector(tvec, edge1);
  float v = getDotProduct(rayDirection, qvec) * inv_det;

  if (v < 0 || u + v > 1)
      return false;

  t = getDotProduct(edge2, qvec) * inv_det;

  return true;
}

void Mesh::loadFromFile(std::string path, int32_t version) {
  std::ifstream file(path);
  if ( ! file.is_open()) {
    std::cout << "NOT FOUND " << path << std::endl;
  } 
  data.clear();
  hitBox.clear();

  Vec4 color;
  std::vector<Vec4> vec;
  float maxValue = 9999999.0;
  float minValue = -9999999.0;
  Vec4 limitLower = { maxValue, maxValue, maxValue, 1.0f };
  Vec4 limitHigher = { minValue, minValue, minValue, 1.0f };
  while ( ! file.eof()) {
    char line[128];
    file.getline(line, 128);
    std::stringstream ss;
    ss << line;

    char junk;
    if (line[ 0 ] == 'v') {
      Vec4 v;
      ss >> junk >> v.x >> v.y >> v.z;
      vec.push_back(v);

      limitLower.x =
        v.x < limitLower.x ? v.x : limitLower.x;
      limitLower.y =
        v.y < limitLower.y ? v.y : limitLower.y;
      limitLower.z =
        v.z < limitLower.z ? v.z : limitLower.z;

      limitHigher.x =
        v.x > limitHigher.x ? v.x : limitHigher.x;
      limitHigher.y =
        v.y > limitHigher.y ? v.y : limitHigher.y;
      limitHigher.z =
        v.z > limitHigher.z ? v.z : limitHigher.z;

    }
    if (line[ 0 ] == 'u') {
      ss >> junk >> color.x >> color.y >> color.z;
    }
    if (line[ 0 ] == 'f') {
      if (version == 2) {
        std::string fall[4];
        ss >> junk >> fall[ 0 ] >> fall[ 1 ] >> fall[ 2 ] >> fall[ 3 ];
        int f[ 4 ];
        getOnlyVertex(fall[ 0 ], f[ 0 ]);
        getOnlyVertex(fall[ 1 ], f[ 1 ]);
        getOnlyVertex(fall[ 2 ], f[ 2 ]);
        getOnlyVertex(fall[ 3 ], f[ 3 ]);
        Triangle tri1;
        tri1.p[ 0 ] = vec[ f[0] - 1 ];
        tri1.p[ 1 ] = vec[ f[1] - 1 ];
        tri1.p[ 2 ] = vec[ f[2] - 1 ];
        tri1.color = color;
        Triangle tri2;
        tri2.p[ 0 ] = vec[ f[2] - 1 ];
        tri2.p[ 1 ] = vec[ f[3] - 1 ];
        tri2.p[ 2 ] = vec[ f[0] - 1 ];
        tri2.color = color;
        data.push_back(tri1);
        data.push_back(tri2);

      } else if (version == 1) {
        int f[3];
        ss >> junk >> f[ 0 ] >> f[ 1 ] >> f[ 2 ];
        Triangle tri;
        tri.p[0] = vec[ f[0] - 1 ];
        tri.p[1] = vec[ f[1] - 1 ];
        tri.p[2] = vec[ f[2] - 1 ];
        data.push_back(tri);
      }
    }
  }

  // calculate hitbox
  hitBox = {{
    // south
    {
      limitLower.x, limitLower.y, limitLower.z, 1.0,
      limitLower.x, limitHigher.y, limitLower.z, 1.0,
      limitHigher.x, limitHigher.y, limitLower.z, 1.0
    },
    {
      limitLower.x, limitLower.y, limitLower.z, 1.0,
      limitHigher.x, limitHigher.y, limitLower.z, 1.0,
      limitHigher.x, limitLower.y, limitLower.z, 1.0
    },

    // west
    {
      limitLower.x, limitLower.y, limitHigher.z, 1.0,
      limitLower.x, limitHigher.y, limitHigher.z, 1.0,
      limitLower.x, limitHigher.y, limitLower.z, 1.0
    },
    {
      limitLower.x, limitLower.y, limitHigher.z, 1.0,
      limitLower.x, limitHigher.y, limitLower.z, 1.0,
      limitLower.x, limitLower.y, limitLower.z, 1.0
    },

    // east
    {
      limitHigher.x, limitLower.y, limitLower.z, 1.0,
      limitHigher.x, limitHigher.y, limitLower.z, 1.0,
      limitHigher.x, limitHigher.y, limitHigher.z, 1.0
    },
    {
      limitHigher.x, limitLower.y, limitLower.z, 1.0,
      limitHigher.x, limitHigher.y, limitHigher.z, 1.0,
      limitHigher.x, limitLower.y, limitHigher.z, 1.0
    },

    // north
    {
      limitHigher.x, limitLower.y, limitHigher.z, 1.0,
      limitHigher.x, limitHigher.y, limitHigher.z, 1.0,
      limitLower.x, limitHigher.y, limitHigher.z, 1.0
    },
    {
      limitHigher.x, limitLower.y, limitHigher.z, 1.0,
      limitLower.x, limitHigher.y, limitHigher.z, 1.0,
      limitLower.x, limitLower.y, limitHigher.z, 1.0
    },

    // top
    {
      limitLower.x, limitHigher.y, limitLower.z, 1.0,
      limitLower.x, limitHigher.y, limitHigher.z, 1.0,
      limitHigher.x, limitHigher.y, limitHigher.z, 1.0
    },
    {
      limitLower.x, limitHigher.y, limitLower.z, 1.0,
      limitHigher.x, limitHigher.y, limitHigher.z, 1.0,
      limitHigher.x, limitHigher.y, limitLower.z, 1.0
    },

    // bottom
    {
      limitLower.x, limitLower.y, limitHigher.z, 1.0,
      limitLower.x, limitLower.y, limitLower.z, 1.0,
      limitHigher.x, limitLower.y, limitLower.z, 1.0
    },
    {
      limitLower.x, limitLower.y, limitHigher.z, 1.0,
      limitHigher.x, limitLower.y, limitLower.z, 1.0,
      limitHigher.x, limitLower.y, limitHigher.z, 1.0
    }
  }};
}


void Mesh::getOnlyVertex(std::string data, int& v) {
  std::string d;
  for (char& c : data) {
    if (c == '/') {
      v = atoi(d.c_str());
      return;
    }

    d += c;
  }
}










#include "Sheep.h"

Sheep::Sheep() {}
Sheep::~Sheep() {}


void Sheep::selected(bool v, Vec4 hitPoint) {

  StageInfo::get()->sheepSelected = v;
  if ( ! v ) {
    extraLight = 0.0f;
    return;
  }

  extraLight = 0.4f;
  std::cout << "sheep selected" << std::endl;

  StageInfo::get()->sheepPos = position;

}


